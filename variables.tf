variable "tenant_id" {
  description = "Rackspace Account Number"
  type        = string
}

variable "user_name" {
  description = "Rackspace Username"
  type        = string
}

variable "password" {
  description = "Rackspace Password"
  type        = string
}

variable "api_key" {
  description = "Rackspace API key"
  type        = string
}

variable "region" {
  description = "Server Region"
  type        = string
  default     = "IAD"
}

variable "gitlab_url" {
  description = "GitLab URL"
  type        = string
  default     = "https://gitlab.com/"
}

variable "gitlab_token" {
  description = "GitLab Runner Registration Token"
  type        = string
}

variable "gitlab_docker_image" {
  description = "GitLab CI Docker Runner Default Docker Image"
  type        = string
  default     = "ruby:2.1"
}

variable "runners_concurrent" {
  description = "GitLab CI Docker concurrent parameter"
  type        = string
  default     = "1"
}

variable "runners_limit" {
  description = "GitLab CI Docker limit parameter"
  type        = string
  default     = "1"
}

variable "runners_instance_flavor" {
  description = "Instance/VM hardware type to use for the runners"
  type        = string
  default     = "general1-4"
}
