Xen CI
======

This repo contains [terraform.io](https://terraform.io) configs for the
Xen Project GitLab CI infrastructure.

Usage
-----

- Install `terraform` for your distro on your machine.
- Terraform state is stored using the [https://www.terraform.io/docs/backends/types/swift.html](swift backend).  To configure this create `backend.conf` with
the following:
  ```
  user_name = "yourname"
  password = "yourpassword"
  tenant_id = 123456
  ```
- Create the config for the plan, create `terraform.tfvars` that looks like:
  ```
  user_name = "yourname"
  password = "yourpassword"
  tenant_id = 123456
  api_key = "key-here"
  gitlab_token = "some-value-here"
  ```
  - `tenant_id` - login to https://mycloud.rackspace.com and get the Account #
  - `api_key` - login above and get it under My Profile and Settings
  - `gitlab_token` - login to https://gitlab.com/xen-project/-/settings/ci_cd to get it
- Get shared SSH provision key at `key/id_provision_key`. (TODO: Cloud Files)
- Run `terraform init -backend-config=terraform.tfvars`
- Run `terraform plan` to see the changes.
- Run `terraform apply` to make the changes.

Updating Terraform
------------------

Periodically there are updates to Terraform and its provider plugins. Run the
following to apply the necessary changes to the state and the local provider
plugins.

- `terraform init -backend-config=terraform.tfvars -upgrade`


One Time Action
---------------

- Generate SSH key at `key/id_provision_key`. e.g.
```
ssh-keygen -t rsa -b 4096 -f key/id_provision_key
```
