#!/bin/sh -ex

export DEBIAN_FRONTEND=noninteractive

# Install gitlab runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
export DEBIAN_FRONTEND=noninteractive
apt-get -qy update
apt-get -qy install gitlab-runner docker.io
apt-get -qy dist-upgrade
apt-get -qy autoremove --purge

mkdir -p /etc/ssh/sshd_config.d
cat <<EOF >/etc/ssh/sshd_config.d/root-login.conf
PermitRootLogin without-password
PasswordAuthentication no
EOF
systemctl reload ssh

apt-get -qy install chiark-scripts

# Newer version of the package, with:
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1040476
wget https://xenbits.xen.org/people/aperard/chiark-utils/chiark-scripts_7.0.0+nmu1~anthony_all.deb
apt-get -qy install ./chiark-scripts_7.0.0+nmu1~anthony_all.deb

adduser --system --group --home /var/cache/git-cache-proxy --disabled-login --disabled-password git-cache
systemctl enable --now git-cache-proxy.socket git-cache-proxy-housekeeping.timer
systemctl enable --now gitlab-runner-clear-docker-cache.timer
systemctl enable --now gitlab-runner-clear-docker-cache-low-space.timer
systemctl enable gitlab-runner

# Make sure docker doesn't restart during package upgrade.
debconf-set-selections <<EOF
debconf docker.io/restart select false
EOF

# Check if reboot is needed once a week
systemctl enable --now reboot-when-required.timer
