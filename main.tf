provider "openstack" {
  user_name = var.user_name
  tenant_id = var.tenant_id
  password  = var.password
  auth_url  = "https://identity.api.rackspacecloud.com/v2.0/"
  region    = var.region
}

resource "random_pet" "name" {
  count     = var.runners_concurrent
  separator = "1"
  length    = "1"

  keepers = {
    public_key = file("key/id_provision_key.pub")
  }
}

resource "openstack_compute_keypair_v2" "provision_key" {
  name       = "provision-key-${random_pet.name[0].id}"
  public_key = random_pet.name[0].keepers.public_key
  region     = var.region
}

data "template_file" "runners_config" {
  template = file("templates/config.toml.tpl")

  vars = {
    runners_concurrent = 1
  }
}

data "openstack_images_image_v2" "ubuntu" {
  name        = "Ubuntu 20.04 LTS (Focal Fossa) (Cloud)"
  most_recent = true
  region      = var.region
}

resource "openstack_compute_instance_v2" "gitlab-docker" {
  count     = length(random_pet.name)
  name      = "gitlab-docker-${random_pet.name[count.index].id}"
  region    = var.region
  image_id  = data.openstack_images_image_v2.ubuntu.id
  flavor_id = var.runners_instance_flavor
  key_pair  = openstack_compute_keypair_v2.provision_key.name
  user_data = file("cloud-init.cfg")

  network {
    uuid = "00000000-0000-0000-0000-000000000000"
    name = "public"
  }

  network {
    uuid = "11111111-1111-1111-1111-111111111111"
    name = "private"
  }

  # SSH access to the host
  connection {
    type        = "ssh"
    host        = self.access_ip_v4
    private_key = file("key/id_provision_key")
  }

  # Wait for cloud-init to update the new instance and reboot it if needed
  provisioner "remote-exec" {
    # When the instance is rebooted by cloud-init, this provisioner fails,
    # ignore the failure and keep going.
    on_failure = continue
    inline = [
      "cloud-init status --wait",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /etc/gitlab-runner/",
    ]
  }

  provisioner "file" {
    content     = data.template_file.runners_config.rendered
    destination = "/etc/gitlab-runner/config.toml"
  }

  provisioner "file" {
    source      = "config-files/systemd-units/"
    destination = "/etc/systemd/system/"
  }

  provisioner "file" {
    source      = "config-files/apt.conf.d/"
    destination = "/etc/apt/apt.conf.d/"
  }

  provisioner "remote-exec" {
    script = "scripts/setup-bastion-machine.sh"
  }

  provisioner "file" {
    content = templatefile("templates/provision-runners.tpl", {
      token              = var.gitlab_token
      gitlab_url         = var.gitlab_url
      docker_image       = var.gitlab_docker_image
      machine_name       = self.name
      runner_limit       = var.runners_limit
    })
    destination = "/tmp/provision-ospc-runners"
  }

  provisioner "remote-exec" {
    inline = [
      "sh /tmp/provision-ospc-runners",
    ]
  }

  provisioner "remote-exec" {
    when       = destroy
    on_failure = continue

    inline = [
      "systemctl stop gitlab-runner || :",
      "gitlab-runner unregister --name ${self.name}",
    ]
  }
}

output "gitlab-docker-ips" {
  value = openstack_compute_instance_v2.gitlab-docker[*].access_ip_v4
}
