#!/bin/bash -e

gitlab-runner register --non-interactive \
    -r ${token} \
    --locked \
    --url ${gitlab_url} \
    --executor docker \
    --run-untagged \
    --tag-list x86,x86_64,x86_32,docker \
    --docker-image ${docker_image} \
    --limit ${runner_limit} \
    --pre-build-script 'if command -v git; then for p in http https git; do git config --global url."git://172.17.0.1:9419/$p://".insteadOf "$p://"; done; fi' \
    --name ${machine_name} \
    --docker-privileged

# cleanup
rm -f $0
